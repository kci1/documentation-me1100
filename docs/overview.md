ME1100 platform
===============
![](attachments/786530318/834405628.jpg)
The goal of multi-access edge computing is to decrease network congestion and improve the performance of applications or workloads by getting task processing closer to the user.

Our mobile edge Xeon® D servers enable content and applications to reside closer to the edge. This allows operators to solve challenges related to restricted space and power while reducing overall costs. The ME1100 1U platform enables edge applications based on Radio Access Network, artificial intelligence, data caching, ultra-low latency and high-bandwidth, among others.

Main applications 
------------------

*   High-performance server for multi-access edge computing (MEC)
*   Enabling IT and cloud-computing capabilities within the Radio Access Network (RAN)
*   Ideal for ultra-low latency and high-bandwidth applications
*   Storage and extension slot for artificial intelligence or data caching applications  
    

Main features
-------------

*   Extended operating temperature range: -40°C to 65°C
*   19-in 1U rackmount, 300-mm deep
    
*   Intel® Xeon® D-1500 processor series (8C, 12C)
*   Two embedded 10 GbE SFP network interfaces 
*   One FHHL or FH3/4L PCIe add-in card
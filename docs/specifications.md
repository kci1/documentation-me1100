ME1100 : Specifications
==========================


___{This article details dimensions, shipping weights, environmental specifications and power consumption and lists key hardware and software features.}___

* * *

ME1100 key hardware features
----------------------------

Feature

Description

Hardware platform

*   High-performance server for mobile edge computing (MEC), 1U height, 300-mm deep, 19 inches wide
*   Hot-swappable fan tray and filter
*   Front access only (motherboard I/O, PSU, PCIe add-in card I/O, fan tray)

I/O

*   Two 10 GbE SFP+
*   One USB 2.0
*   One RJ45 10/100/1000Base-T management port
*   One RJ45 serial port
*   One reset button

PCIe add-in card

*   One optional FHHL or FH¾L PCIe x16 add-in card supported (power and thermal restrictions may apply)
*   Maximum power consumption supported is 50 W

Refer to the [Hardware compatibility list]()

CPU

Intel® Xeon® D-1500 family processors are supported, including the following processors:

*   Xeon® D-1548, 8 Cores @ 2.00GHz, 45W
*   Xeon® D-1567, 12 Cores @ 2.10GHz, 65W
*   Xeon® D-1559, 12 Cores @ 1.50GHz, 45W

Drive 

One M.2 SATA SSD up to 2280 option: 

Refer to the [Hardware compatibility list]() 

Four 2.5-in SATA SSD option:

Refer to the [Hardware compatibility list]()

Memory

DDR4 DIMM with ECC 

*   Bandwidth up to 2400 MT/s
*   Two memory channels
*   One DIMM socket per channel

Refer to the [Hardware compatibility list]()

Power inlet

One -57 VDC to -40 VDC dual input feed

Power consumption

Refer to [Power consumption and power budget]()

Fans

Hot-swappable fan tray:

*   Fan tray contains 3 fans (standard configuration)
*   Fan tray contains 4 fans when 2.5-in SATA SSD option is installed
*   Fan filter can be removed independently from the fan tray
*   Automatic fan speed control

Rack mounting brackets

Front or middle mount in a 19-in wide rack

ME1100 key software features
----------------------------

Feature

Description

Platform management

*   Integrated BMC – this subsystem consists of communication buses, sensors, system BIOS, and server management firmware; it supports standard IPMI features as well as OEM (supplemental) features that are not part of IPMI
*   IPMI-based system monitor used for server monitoring, diagnostic and configuration
*   System event log
*   Server power consumption monitoring
*   Server power control
*   Server and component health monitoring
*   Fan speed monitoring
*   Serial over LAN console access
*   IPMI over LAN
*   Sensor data record describing all sensors and providing their readings (analog or discrete)
*   ACPI state synchronization: the BMC tracks ACPI state changes that are provided by the BIOS
*   BIOS recovery

Operating system

Refer to the [Validated operating systems]()

Thermal management

*   Platform Environment Control Interface (PECI) for thermal management support
*   Memory and CPU thermal management

ME1100 physical dimensions
--------------------------

Chassis

Measurements (mm \[in\])

Notes

Depth

300 \[11.8\]

Body

Width

449 \[17.6\] max.

Body

483 \[19\] max.  

Overall width: front mounting brackets included (2 times 17.2 mm \[0.7 in\])  

465 \[18.3\]

Between rack mounting points

Height

43.5 \[1.7\] max.  

Body

Side clearance

70 \[2.8\]

Between rack mounting points

Front clearance

100 \[4\]

Recommended

Rear clearance

None

  

ME1100 packaging physical dimensions
------------------------------------

Depth (mm \[in\])

Width (mm \[in\])

Height (mm \[in\])

422 \[16.6\]

605 \[23.8\]

170 \[6.7\]

ME1100 shipping weights
-----------------------

Component

Weight (kg)

Weight (lb)

System weight – with two DIMM and one M.2-2280 SATA SSD

4.37  

9.63  

Packaging (box + foam + bag)

1.124  

2.48  

2.5-in SSD carrier bracket

0.206  

0.45  

2.5-in SATA SSD (1)

0.060  

0.13  

ME1100 environmental specifications
-----------------------------------

Environment

Specification

Temperature, operating

\-40ºC to +65ºC (-40ºF to +149ºF)   
The failure of one fan will not impact operation for at least 4 hours at 65˚C.   
Certain limitations may apply. These limitations could be the result of the operating temperature range of installed configurable components (e.g., SFP+ module, SSD and PCIe add-in card). Kontron recommends using SFP+ and SSD modules with an industrial operating temperature range (-40°C to +85°C). Another limitation can result from airflow obstruction caused by fan filter clogging and failure to follow recommended side clearances.

Temperature, non-operating

\-40ºC to +70ºC (-40ºF to +158ºF)

Humidity, operating

5% to 95%, non-condensing

Altitude/pressure, operating

\-60 m to 1,800 m altitude without temperature de-rating   
Up to 4,000 m altitude with temperature de-rating of 1 degree Celsius per 300 m above 1,800 m

Altitude/pressure, non-operating

Up to 4,570 m

Vibration, operating

This product meets operational random vibration

Test profile based on ETSI EN 300 019-2-3 class 3.2

*   5 Hz to 10 Hz at +12 dB/octave (slope up)
*   10 Hz to 50 Hz at 0.02 m2/s3 (0.0002 g2/Hz) (flat)
*   50 Hz to 100 Hz at -12 dB/octave (slope down)
*   30 minutes for each of the three axes

Vibration, non-operating

This product meets transportation and storage random vibration

Test profile based on GR-63 clause 5.4.3, and ETSI EN 300 019-2-2 class 2.3

*   5 Hz to 20 Hz at 1 m2/s3 (0.01 g2/Hz) (flat)
*   20 Hz to 200 Hz at -3 dB/octave (slope down)
*   30 minutes for each of the three axes

Shock, operating

This product meets operational shock standards

Test profile based on ETSI EN 300 019-2-3 class 3.2

*   11 ms half sine, 3 g, three shocks in each direction

Drop/free fall

This product meets Bellcore GR-63 section 5.3

Packaged = 1,000 mm, six surfaces, three edges and four corners

Unpackaged = 100 mm, two sides and two bottom corners

Electrostatic discharge

This product meets 8 kV contact, 15 kV air discharge using IEC 61000-4-2 test method

RoHS and WEEE

This product is designed to meet China RoHS Phase 1 (self-declaration and labeling)

This product complies with EU directive 2012/19/EU (WEEE)

This product complies with RoHS directive 2011/65/EU as modified by EU 2015/863

#Planning

The goal of multi-access edge computing is to decrease network congestion and improve the performance of applications or workloads by getting task processing closer to the user.

Our mobile edge Xeon® D servers enable content and applications to reside closer to the edge. This allows operators to solve challenges related to restricted space and power while reducing overall costs. The ME1100 1U platform enables edge applications based on Radio Access Network, artificial intelligence, data caching, ultra-low latency and high-bandwidth, among others.

Main applications 
------------------

*   High-performance server for multi-access edge computing (MEC)
*   Enabling IT and cloud-computing capabilities within the Radio Access Network (RAN)
*   Ideal for ultra-low latency and high-bandwidth applications
*   Storage and extension slot for artificial intelligence or data caching applications  